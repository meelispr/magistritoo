package ee.ttu.magistritoo.util;

import java.util.Comparator;

import ee.ttu.magistritoo.domain.ConformityValue;

public class DataminingUtil{

    public static void emptyCheck( int[][] table ){
        if( table == null || table.length == 0 ){
            throw new RuntimeException( "Table empty" );
        }
    }

    public static ConformityValue findFirstValue( int[][] conformityTable, Comparator<Integer> comparator ){
        emptyCheck( conformityTable );
        ConformityValue min = new ConformityValue( 0, 0, Integer.MAX_VALUE );
        for( int rowIdx = 0; rowIdx < conformityTable.length; rowIdx++ ){
            for( int colIdx = 0; colIdx < conformityTable[rowIdx].length; colIdx++ ){
                int value = conformityTable[rowIdx][colIdx];
                if( value != 0 && comparator.compare( min.getConformity(), value ) > 0 ){
                    min.setColIdx( colIdx );
                    min.setRowIdx( rowIdx );
                    min.setValue( value );
                }
            }
        }
        return min;
    }
}
