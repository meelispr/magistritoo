package ee.ttu.magistritoo;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import ee.ttu.magistritoo.domain.ClosedSetWithGenerator;
import ee.ttu.magistritoo.domain.ConformityValue;
import ee.ttu.magistritoo.domain.Generator;
import ee.ttu.magistritoo.util.DataminingUtil;
import ee.ttu.magistritoo.util.MinComparator;

//TODO: add multiple thread support with java7 or even try 8
public class Main{

    private static int differentValues = 3;

    private static final Comparator<Integer> minComparator = new MinComparator();

    public List<ClosedSetWithGenerator> calculate( int[][] table, int numberOfClosedSets ){
        List<ClosedSetWithGenerator> closedSetWithGenerators = new ArrayList<ClosedSetWithGenerator>();
        int[] extractedRows = new int[table.length];
        for( int i = 0; i < table.length; i++ ){
            extractedRows[i] = i;
        }
        int[][] conformity = calculateConformity( table );
        for( int i = 0; i < numberOfClosedSets; i++ ){
            Generator generator = new Generator();
            findClosedSet( closedSetWithGenerators, table, extractedRows, conformity, generator );
        }
        return closedSetWithGenerators;
    }

    protected void findClosedSet( List<ClosedSetWithGenerator> closedSetWithGenerators,
                                  int[][] table,
                                  int[] extractedRowsIdx,
                                  int[][] conformity,
                                  Generator generator ){
        ConformityValue minConformity = extractMinConformity( conformity );
        generator.addLast( minConformity.getTableColIdx(), minConformity.getTableColValue() );
        extractedRowsIdx = extractRows( table, minConformity, extractedRowsIdx );
        int[][] extractedConformity = calculateConformity( table, conformity, extractedRowsIdx );
        //TODO: remove minimalClosedSet and add higher level generator and closed set to result
        boolean minimalClosedSet = true;
        while( findMinConformity( extractedConformity ).getConformity() < minConformity.getConformity() ){
            minimalClosedSet = false;
            findClosedSet( closedSetWithGenerators, table, extractedRowsIdx, extractedConformity, generator );
        }
        //TODO: if extracted Rows are objects then i can just add the generator to be able to get all the generators;
        if( minimalClosedSet ){
            addResult( closedSetWithGenerators, extractedRowsIdx, generator );
        }
    }

    private void addResult( List<ClosedSetWithGenerator> closedSetWithGenerators, int[] extractedRowsIdx, Generator generator ){
        //dont add duplicates add generator to existing closed set
        closedSetWithGenerators.add( new ClosedSetWithGenerator( generator.makeFinalCopy(), extractedRowsIdx ) );
        generator.removeLast();
    }

    protected int[][] calculateConformity( int[][] table ){
        //TODO: sort by value first then i can just iterate through
        //TODO: maybe change to object if necessary
        DataminingUtil.emptyCheck( table );
        int[][] result = new int[table[0].length][differentValues];
        for( int[] row : table ){
            for( int colIdx = 0; colIdx < row.length; colIdx++ ){
                addRowConformity( result, colIdx, row[colIdx] );
            }

        }
        return result;
    }

    protected int[] extractRows( int[][] table, ConformityValue minConformity, int[] extractedRowsIdx ){
        DataminingUtil.emptyCheck( table );
        int[] result = new int[minConformity.getConformity()];
        int resultIdx = 0;
        for( int tableRowIdx : extractedRowsIdx ){
            if( table[tableRowIdx][minConformity.getTableColIdx()] == minConformity.getTableColValue() ){
                result[resultIdx++] = tableRowIdx;
            }
        }
        return result;
    }

    protected int[][] calculateConformity( int[][] table, int[][] existingConformity, int[] extractedRowsIdx ){
        //TODO: Don't use columns that have already been used
        DataminingUtil.emptyCheck( table );
        int[][] result = new int[table[0].length][differentValues];
        for( int rowIdx : extractedRowsIdx ){
            int[] row = table[rowIdx];
            for( int colIdx = 0; colIdx < row.length; colIdx++ ){
                int value = row[colIdx];
                if( existingConformity[colIdx][value - 1] > 0 ){
                    addRowConformity( result, colIdx, value );
                }
            }
        }
        return result;
    }

    private void addRowConformity( int[][] result, int colIdx, int value ){
        result[colIdx][value - 1]++;
    }

    protected static ConformityValue extractMinConformity( int[][] conformityTable ){
        ConformityValue conformityValue = findMinConformity( conformityTable );
        conformityTable[conformityValue.getRowIdx()][conformityValue.getColIdx()] = 0;
        return conformityValue;
    }

    private static ConformityValue findMinConformity( int[][] conformityTable ){
        return DataminingUtil.findFirstValue( conformityTable, minComparator );
    }

}
