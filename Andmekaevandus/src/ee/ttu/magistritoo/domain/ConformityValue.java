package ee.ttu.magistritoo.domain;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class ConformityValue{
    private int rowIdx;
    private int colIdx;
    private int value;

    public ConformityValue( int rowIdx, int colIdx, int value ){
        this.rowIdx = rowIdx;
        this.colIdx = colIdx;
        this.value = value;
    }

    public int getRowIdx(){
        return rowIdx;
    }

    public void setRowIdx( int rowIdx ){
        this.rowIdx = rowIdx;
    }

    public int getColIdx(){
        return colIdx;
    }

    public void setColIdx( int colIdx ){
        this.colIdx = colIdx;
    }

    public int getConformity(){
        return value;
    }

    public void setValue( int value ){
        this.value = value;
    }

    public int getTableColIdx(){
        return rowIdx;
    }

    public int getTableColValue(){
        return colIdx + 1;
    }

    @Override
    public int hashCode(){
        final int prime = 31;
        int result = 1;
        result = prime * result + colIdx;
        result = prime * result + rowIdx;
        result = prime * result + value;
        return result;
    }

    @Override
    public boolean equals( Object obj ){
        if( this == obj ){
            return true;
        }
        if( obj == null ){
            return false;
        }
        if( getClass() != obj.getClass() ){
            return false;
        }
        ConformityValue other = (ConformityValue)obj;
        if( colIdx != other.colIdx ){
            return false;
        }
        if( rowIdx != other.rowIdx ){
            return false;
        }
        if( value != other.value ){
            return false;
        }
        return true;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString( this );
    }
}
