package ee.ttu.magistritoo.domain;

import java.util.ArrayList;
import java.util.List;

public class ClosedSetWithGenerator{
    private List<Generator> generators = new ArrayList<Generator>();
    private int[] closedSet;

    public ClosedSetWithGenerator( Generator generator, int[] closedSet ){
        addGenerator( generator );
        this.closedSet = closedSet;
    }

    public void addGenerator( Generator generator ){
        this.generators.add( generator );
    }

    public int[] getClosedSet(){
        return closedSet;
    }

    public List<Generator> getGenerators(){
        return generators;
    }
}
