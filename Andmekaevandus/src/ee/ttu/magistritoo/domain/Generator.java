package ee.ttu.magistritoo.domain;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

public class Generator{
    //TODO: replace with ArrayDeque
    //TODO: add initial size to first level conformity
    List<GeneratorValue> rowColPairs = new ArrayList<GeneratorValue>();

    public void addLast( int col, int value ){
        this.rowColPairs.add( new GeneratorValue( col, value ) );
    }

    public void removeLast(){
        rowColPairs.remove( rowColPairs.size() - 1 );
    }

    public Generator makeFinalCopy(){
        Generator generator = new Generator();
        generator.rowColPairs = new ArrayList<GeneratorValue>( rowColPairs );
        return generator;
    }

    @Override
    public int hashCode(){
        final int prime = 31;
        int result = 1;
        result = prime * result + ((rowColPairs == null) ? 0 : rowColPairs.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ){
        if( this == obj ){
            return true;
        }
        if( obj == null ){
            return false;
        }
        if( getClass() != obj.getClass() ){
            return false;
        }
        Generator other = (Generator)obj;
        if( rowColPairs == null ){
            if( other.rowColPairs != null ){
                return false;
            }
        }
        else if( !rowColPairs.equals( other.rowColPairs ) ){
            return false;
        }
        return true;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString( this );
    }
}

class GeneratorValue{
    private int col;
    private int value;

    public GeneratorValue( int col, int value ){
        this.col = col;
        this.value = value;
    }

    public int getCol(){
        return col;
    }

    public int getValue(){
        return value;
    }

    @Override
    public int hashCode(){
        final int prime = 31;
        int result = 1;
        result = prime * result + col;
        result = prime * result + value;
        return result;
    }

    @Override
    public boolean equals( Object obj ){
        if( this == obj ){
            return true;
        }
        if( obj == null ){
            return false;
        }
        if( getClass() != obj.getClass() ){
            return false;
        }
        GeneratorValue other = (GeneratorValue)obj;
        if( col != other.col ){
            return false;
        }
        if( value != other.value ){
            return false;
        }
        return true;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString( this );
    }

}
