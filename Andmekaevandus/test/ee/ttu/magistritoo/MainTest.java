package ee.ttu.magistritoo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ee.ttu.magistritoo.domain.ClosedSetWithGenerator;
import ee.ttu.magistritoo.domain.ConformityValue;
import ee.ttu.magistritoo.domain.Generator;

public class MainTest{

    private Main main;
    private int[][] table;
    private ConformityValue minConformity;

    @Before
    public void init(){
        this.main = new Main();
        this.table = new int[][]{
                {1, 3, 1, 2},
                {1, 3, 1, 1},
                {3, 1, 2, 1},
                {1, 2, 1, 2},
                {3, 2, 1, 1}};
        this.minConformity = new ConformityValue( 1, 0, 1 );
    }

    @Test
    public void calculateConformityTest(){
        int[][] expected = {
                {3, 0, 2},
                {1, 2, 2},
                {4, 1, 0},
                {3, 2, 0}};
        Assert.assertArrayEquals( expected, main.calculateConformity( table ) );
    }

    @Test
    public void findMinConformityTest(){
        int[][] table = {
                {3, 0, 2},
                {1, 2, 2},
                {4, 1, 0},
                {3, 2, 0}};
        Assert.assertEquals( minConformity, Main.extractMinConformity( table ) );
    }

    @Test
    public void extractRowsTest(){
        int[] expected = {2};
        int[] extractedRowsIdx = {0, 1, 2, 3, 4};
        Assert.assertArrayEquals( expected, main.extractRows( table, minConformity, extractedRowsIdx ) );
    }

    @Test
    public void extractRowsWithAlreadyExtractedTest(){
        this.minConformity = new ConformityValue( 1, 1, 1 );
        int[] expected = {3};
        int[] extractedRowsIdx = {0, 1, 2, 3};
        Assert.assertArrayEquals( expected, main.extractRows( table, minConformity, extractedRowsIdx ) );
    }

    @Test
    public void calculateExtractedRowConformityTest(){
        int[] extractedRowsIdx = new int[]{2};
        int[][] expected = {
                {0, 0, 1},
                {1, 0, 0},
                {0, 1, 0},
                {1, 0, 0}};
        int[][] existingConformity = {
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}};

        Assert.assertArrayEquals( expected, main.calculateConformity( table, existingConformity, extractedRowsIdx ) );
    }

    @Test
    public void calculateExtractedRowConformityWithExistingConformityEliminationTest(){
        int[] extractedRowsIdx = new int[]{2};
        int[][] expected = {
                {0, 0, 0},
                {1, 0, 0},
                {0, 1, 0},
                {1, 0, 0}};
        int[][] existingConformity = {
                {1, 1, 0},
                {1, 1, 1},
                {1, 1, 1},
                {1, 1, 1}};

        Assert.assertArrayEquals( expected, main.calculateConformity( table, existingConformity, extractedRowsIdx ) );
    }

    @Test
    public void findFirstClosedSetWithGeneratorTest(){
        Generator expectedGenerator = new Generator();
        expectedGenerator.addLast( 1, 1 );
        int[] expectedClosedSet = {2};

        ClosedSetWithGenerator result = main.calculate( table, 1 ).get( 0 );
        Assert.assertEquals( expectedGenerator, result.getGenerators().get( 0 ) );
        Assert.assertArrayEquals( expectedClosedSet, result.getClosedSet() );
    }

    @Test
    public void findSecondClosedSetWithGeneratorTest(){
        Generator expectedGenerator = new Generator();
        expectedGenerator.addLast( 2, 2 );
        int[] expectedClosedSet = {2};

        ClosedSetWithGenerator result = main.calculate( table, 2 ).get( 1 );
        Assert.assertEquals( expectedGenerator, result.getGenerators().get( 0 ) );
        Assert.assertArrayEquals( expectedClosedSet, result.getClosedSet() );
    }

    @Test
    public void findGreaterDepthClosedSetWithGeneratorTest(){
        Generator expectedGenerator = new Generator();
        expectedGenerator.addLast( 0, 3 );
        expectedGenerator.addLast( 1, 2 );
        int[] expectedClosedSet = {4};

        ClosedSetWithGenerator result = main.calculate( table, 3 ).get( 2 );
        Assert.assertEquals( expectedGenerator, result.getGenerators().get( 0 ) );
        Assert.assertArrayEquals( expectedClosedSet, result.getClosedSet() );
    }

    @Test
    public void findMultypleClosedSetWithGeneratorTest(){
        Generator expectedGenerator = new Generator();
        expectedGenerator.addLast( 0, 3 );
        expectedGenerator.addLast( 2, 1 );
        int[] expectedClosedSet = {4};

        ClosedSetWithGenerator result = main.calculate( table, 3 ).get( 3 );
        Assert.assertEquals( expectedGenerator, result.getGenerators().get( 0 ) );
        Assert.assertArrayEquals( expectedClosedSet, result.getClosedSet() );
    }

    @Test
    public void dontExtractSameFreqElementsTest(){
        Generator expectedGenerator = new Generator();
        expectedGenerator.addLast( 1, 2 );
        expectedGenerator.addLast( 0, 1 );
        int[] expectedClosedSet = {3};

        ClosedSetWithGenerator result = main.calculate( table, 4 ).get( 4 );
        Assert.assertEquals( expectedGenerator, result.getGenerators().get( 0 ) );
        Assert.assertArrayEquals( expectedClosedSet, result.getClosedSet() );
    }
}
